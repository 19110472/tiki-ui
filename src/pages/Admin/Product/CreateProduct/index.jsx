import React from 'react'
import {
  Box,
  Typography,
  Stack,
  Button,
  TextField,
  Grid,
  Autocomplete,
  TextareaAutosize
} from '@mui/material';
import "./CreateProduct.scss"
import AddIcon from '@mui/icons-material/Add';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import PlayCircleOutlineIcon from '@mui/icons-material/PlayCircleOutline';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { toast } from "react-toastify";
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import ErrorIcon from '@mui/icons-material/Error';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';

const choose = [
  { label: 'The Shawshank Redemption' },
  { label: 'The Godfather' },
  { label: 'The Godfather: Part II' },
  { label: 'The Dark Knight' },
  { label: '12 Angry Men' },
  { label: "Schindler's List" },
  { label: 'Pulp Fiction' }];
export default function CreateProduct() {
  const handleSave = () => {
    toast.warning("Vui lòng nhập đầy đủ thông tin !!");
}
  return (
    <Box px={2} spacing={2}>
      <Stack>
        <Stack sx={{ backgroundColor: "#FFF", height: "80px" }} px={2} direction="row" justifyContent="flex-start" alignItems="center" mb={0.2}>
          <ArrowBackIcon />
          <Typography sx={{ fontWeight: "bold" }}>Tạo sản phẩm mới</Typography>
        </Stack>
        <Stack>
          <Stack sx={{ backgroundColor: "#FFF", height: "40px" }} px={2} direction="row" justifyContent="space-between" alignItems="center">
            <Typography sx={{ fontWeight: "bold" }}>1.Thông tin chung</Typography>
          </Stack>
          <Stack sx={{ backgroundColor: "#FFF", height: "360px" }} px={2} mt={0.2} alignItems="flex-start">
            <Stack spacing={1}>
              <Typography sx={{ fontWeight: "bold" }} mt={2}>* Tên sản phẩm</Typography>
              <TextField size="small" sx={{ width: "100%" }} multiline = "true"></TextField>
              <Typography sx={{ fontWeight: "bold" }} mt={2}>* Danh mục</Typography>
              <Autocomplete size="small" disablePortal id="1" options={choose} sx={{ width: "100%" }} renderInput={(params) => <TextField {...params} />} />
              <Stack direction="row" justifyContent="space-between" alignItems="flex-end">
                <Grid container spacing={2} columns={2} mt={1} >
                  <Grid item xs={1}>
                    <Stack direction="row" spacing={0.5} >
                      <Typography mb={1} sx={{ fontWeight: "bold", fontSize: "14px" }}>* Thương hiệu</Typography>
                      <ErrorOutlineIcon />
                    </Stack>
                    <Autocomplete size="small" disablePortal id="1" options={choose} sx={{ width: "100%" }} renderInput={(params) => <TextField {...params} />} />
                  </Grid>
                  <Grid item xs={1}>
                    <Typography mb={1} sx={{ fontWeight: "bold", fontSize: "14px" }}>* Xuất xứ</Typography>
                    <Autocomplete size="small" disablePortal id="1" options={choose} sx={{ width: "100%" }} renderInput={(params) => <TextField {...params} />} />
                  </Grid>
                </Grid>
              </Stack>
              <Box sx={{ backgroundColor: "#e6fff8" }} height="70px" mt={2} borderColor="#e6f7ff" px={2} >
                <Stack direction="row" justifyContent="space-between" alignItems="center" spacing={1} mt={2} >
                  <ErrorIcon sx={{ color: "#1890ff" }} />
                  <Typography>Bạn hãy điền thêm các thuộc tính được đánh dấu bằng KEY (Thông tin sản phẩm chính) để giúp tăng khả năng hiển thị tìm kiếm và chuyển đổi sản phẩm của bạn</Typography>
                </Stack>
              </Box>
            </Stack>
          </Stack>
        </Stack>
      </Stack>
      <Stack sx={{ backgroundColor: "#fff" }}>
        <Stack sx={{ backgroundColor: "#FFF", height: "46px" }} px={2} direction="row" justifyContent="space-between" alignItems="center">
          <Typography sx={{ fontWeight: "bold" }}>2. Mô Tả Sản Phẩm</Typography>
        </Stack>
        <Stack p={2} spacing={2}>
          <Typography sx={{ fontWeight: "bold" }}>Video sản phẩm</Typography>
          <Typography>.mp4, .mkv | Độ dài video tối đa 1,5 phút | Tối đa 15 MB</Typography>
          <Button className="btn__upload" variant="contained" component="label">
            <Stack justifyContent="center" alignItems="center" spacing={2}>
              <PlayCircleOutlineIcon sx={{fontSize:"50px"}} />
              <Typography sx={{ color: "#333" }}>Nhấn hoặc kéo thả tập tin vào để tải lên</Typography>
            </Stack>
            <input hidden accept="image/*" multiple type="file" />
          </Button>
          <Stack direction="row">
            <Typography sx={{ fontWeight: "bold" }}>Mô tả chi tiết sản phẩm (Không chèn link/địa chỉ/SĐT/website/logo nhà bán)</Typography>
            <InfoOutlinedIcon />
          </Stack>
          <TextField size="small" sx={{ width: "100%" }} multiline = "true"></TextField>
        </Stack>
      </Stack>
      <Stack sx={{ backgroundColor: "#fff" }} pt={2}>
        <Stack sx={{ backgroundColor: "#FFF", height: "46px" }} px={2} direction="row" justifyContent="space-between" alignItems="center">
          <Typography sx={{ fontWeight: "bold" }}>3. Giá Bán Và Thuộc Tính</Typography>
        </Stack>
        <Stack sx={{ backgroundColor: "#FFF", height: "46px" }} px={2} direction="row" justifyContent="space-between" alignItems="center">
        <Typography sx={{ fontWeight: "bold" }}>* Giá Bán</Typography>
        </Stack>
        <Stack sx={{ backgroundColor: "#FFF", height: "46px" }} px={2} direction="row" justifyContent="space-between" alignItems="center">
        <TextField/>
        </Stack>
        <Stack sx={{ backgroundColor: "#FFF", height: "46px" }} px={2} direction="row" justifyContent="space-between" alignItems="center">
        <Typography sx={{ fontWeight: "bold" }}>* Thuộc Tính</Typography>
        </Stack>
        <Stack px={2} py={2}>
          <Button className="btn__add" variant="contained" component="label" >
            <AddIcon sx={{color:"#1890ff"}} />
            <Typography sx={{color:"#1890ff"}}>Thêm lựa chọn hàng để giúp khách hàng tìm kiếm sản phẩm và dễ dàng thêm mới lựa chọn</Typography>
          </Button>
        </Stack>
        <Stack px={2} py={2}>
          <Button className="btn__addProduct" variant="contained" component="label" onClick={handleSave}>
            <AddIcon sx={{color:"#ffffff"}} />
            <Typography sx={{color:"#ffffff"}}>Thêm sản phẩm</Typography>
          </Button>
        </Stack>
      </Stack>
    </Box>
  )
}
