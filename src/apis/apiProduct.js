
//import { axiosClient, axiosInstance } from "./axiosClient";
import axios from 'axios';
import queryString from 'query-string';
const baseURL = 'https://nhom3-tiki.herokuapp.com/api'
export const axiosClient = axios.create({
    baseURL: baseURL,
    headers: {
        "Content-Type": "application/json"
    },
    withCredentials: true,
    paramsSerializer: (params) => queryString.stringify(params)
});

const apiProduct = {

    getProductsById: async (id) => {
        const res = await axiosClient.get('/product', { params: { id } })
        return res.data;
    },
    getProductsBySlug: async (slug) => {
        const res = await axiosClient.get('/products', { params: { slug } })
        return res.data;
    },
    getProducts: async () => {
        const res = await axiosClient.get('/product/all')
        return res.data;
    },
    getCategoryFilterById: async (id) => {
        const res = await axiosClient.get(`/category/${id}`)
        return res.data;
    },

    getProductsBySearch: async (params) => {
        const res = await axiosClient.get('/products' + params)
        return res.data
    },
    getProductByCategory: async (params) => {
        const res = await axiosClient.get('/product/byCategory', {params})
        return res.data
    },
    getCategoryChild: async (params) => {
        const res = await axiosClient.get('/category/child', {params})
        return res.data
    }
}
export default apiProduct;