import axios from 'axios';
import queryString from 'query-string';
import {axiosClientWithToken} from "./axiosClient";
const baseURL='https://nhom3-tiki.herokuapp.com/api'
export const axiosClient = axios.create({
    baseURL: baseURL,
    headers: {
        "Content-Type": "application/json"
    },
    withCredentials: true,
    paramsSerializer: (params) => queryString.stringify(params)
});


const apiCart = {
    getOrders: async (params) => {
        const res = await axiosClient.post('/myorders', params)
        return res.data;
    },
    getCartItem: async (params) => {
        const res = await axiosClientWithToken.get('/cart')
        return res.data;
    },
    addToCard : async (params) =>{
        const res = await axiosClientWithToken.put('/cart', params) 
        return res.success;
    },
    saveOrder: async (params) => {
        const res = await axiosClient.post('/myorders',params)
        return res.data;
    },
    changeTypeOrder: async (params, id) => {
        const res = await axiosClient.patch(`/myorders/${id}`,params)
        return res.data;
    }
    
    
}
export default apiCart;